## Context:

https://gitlab.eclipse.org/groups/eclipse-wg/oniro-wg/marketing-oniro/-/wikis/Events_CFP_participation/Open-Source-Summit

https://linuxfoundation.smapply.io/prog/oss_eu_22/

## Speakers

- Philippe Coval
- Eilís Ní Fhlannagáin 

## Title

Using Oniro Blueprints to create music (Even more embedded music !)

## Description

Oniro is an ambitious Eclipse project,
one of its challenges is to defragment existing IoT ecosystems.

In the Eclipse Oniro project, along the distributed OS for consumer electronics,
we're proposing a collection of "blueprints" use cases and their implementations
using embedded software on reference hardware.

Those minimal viable products are not only used for demonstration or validation purposes,
but they can serve as a base to create production-ready solutions.

Early development of Oniro started with basic blueprints from 
touch panel, door lock, keypad to more sophisticated ones like vending machine or IoT gateway.

Each of those achievements are targeting different uses case and different environments,
but many steps can be factorized from building process, customization to
security or IP compliance scanning.

To inspire devices makers,
today we're focusing on recent works done for Oniro "goofy" release 
with a more advanced scenario that show interactions between wearable and a music system.
We'll explain how to replicate it and deploy firmware from sources to devices and 
why Oniro can be flexible to create the device of your dream.

## References

Oniro: The Distributed Operating System That Connects Consumer Devices Big and Small
https://oniroproject.org/

Oniro Videos
https://www.youtube.com/watch?v=p-gSvehb-As&list=PLy7t4z5SYNaQBDReZmeHAknEchYmu0LLa#OniroPlaylist

